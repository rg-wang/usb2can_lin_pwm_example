/**
 * @file   USB2XXX_LIN_ElmosProgramer.cpp
 * @brief  ELMOS芯片烧写范例程序
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-12-03
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "usb_device.h"
#include "elmos_programer.h"
#if _WIN32
#include <windows.h>
#pragma comment(lib,"USB2XXX.lib")
#endif

int main(int argc, const char* argv[])
{
    DEVICE_INFO DevInfo;
    int DevHandleArray[10];
    int LINMasterIndex = 0;
    int DevIndex = 0;
    bool state;
    int ret;
    //扫描连接到电脑的设备，并获取设备号
    ret = USB_ScanDevice(DevHandleArray);
    if(ret <= 0){
        printf("No device connected!\r\n");
        return 0;
    }
    //打开设备
    state = USB_OpenDevice(DevHandleArray[DevIndex]);
    if(!state){
        printf("Open device error!\r\n");
        return 0;
    }
    char FunctionStr[256]={0};
    //获取设备信息
    state = DEV_GetDeviceInfo(DevHandleArray[DevIndex],&DevInfo,FunctionStr);
    if(!state){
        printf("Get device infomation error!\r\n");
        return 0;
    }else{
        printf("Firmware Info:\r\n");
	    printf("Firmware Name:%s\r\n",DevInfo.FirmwareName);
        printf("Firmware Build Date:%s\r\n",DevInfo.BuildDate);
        printf("Firmware Version:v%d.%d.%d\r\n",(DevInfo.FirmwareVersion>>24)&0xFF,(DevInfo.FirmwareVersion>>16)&0xFF,DevInfo.FirmwareVersion&0xFFFF);
        printf("Hardware Version:v%d.%d.%d\r\n",(DevInfo.HardwareVersion>>24)&0xFF,(DevInfo.HardwareVersion>>16)&0xFF,DevInfo.HardwareVersion&0xFFFF);
        printf("Firmware SerialNumber:%08X%08X%08X\r\n",DevInfo.SerialNumber[0],DevInfo.SerialNumber[1],DevInfo.SerialNumber[2]);
    }
    //设置速度
    ret = ELMOS_SetSpeed(DevHandleArray[DevIndex],LINMasterIndex,SPEED_48K);
    if(ret != ELMOS_SUCCESS){
        printf("Set Speed failed!\r\n");
        return 0;
    }else{
        printf("Set Speed success!\r\n");
    }
#ifdef E52139
    printf("Start Program Flash...\r\n");
    ret = ELMOS_StartProg(DevHandleArray[DevIndex],LINMasterIndex,"E52139B0_ROM_CustomConfig_NoAPPCRCCheck_CRC.hex");
    if(ret != ELMOS_SUCCESS){
        printf("Program ROM failed! %d\r\n",ret);
        return 0;
    }else{
        printf("Program ROM success!\r\n");
    }
#endif
    printf("Start Program Flash...\r\n");
    ret = ELMOS_StartProg(DevHandleArray[DevIndex],LINMasterIndex,"E52139A_SampleProject.hex");
    if(ret != ELMOS_SUCCESS){
        printf("Program Flash failed! %d\r\n",ret);
        return 0;
    }else{
        printf("Program Flash success!\r\n");
    }
    USB_CloseDevice(DevHandleArray[DevIndex]);
	return 0;
}

