#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    int DevHandle[20];
    int DevNum = USB_ScanDevice(DevHandle);
    for(int i=0;i<DevNum;i++){
        if(USB_OpenDevice(DevHandle[i])){
            for(int j=0;j<2;j++){
                ui->comboBoxDevCh->addItem(QString("[%1][CAN%2]").arg(DevHandle[i],8,16,QChar('0')).arg(j+1).toUpper());
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButtonInit_clicked()
{
    QString HandleStr=ui->comboBoxDevCh->currentText();
    int start = HandleStr.indexOf("[");
    int end = HandleStr.indexOf("]");
    int CurrentHandle = HandleStr.mid(start+1,end-start-1).toUInt(NULL,16);
    int CurrentChannel = HandleStr.mid(HandleStr.indexOf("[CAN")+4,1).toUInt(NULL)-1;
    int Baudrate = ui->comboBoxBaudrate->currentText().remove("Kbps").toInt()*1000;
    CAN_INIT_CONFIG CanConfig;
    int ret = CAN_GetCANSpeedArg(CurrentHandle,&CanConfig, Baudrate);
    if(ret != CAN_SUCCESS){
        QMessageBox::warning(this,tr("警告"),tr("获取波特率参数失败！"));
        return;
    }
    ret = CAN_Init(CurrentHandle,CurrentChannel,&CanConfig);
    if(ret != CAN_SUCCESS){
        QMessageBox::warning(this,tr("警告"),tr("初始化CAN失败！"));
        return;
    }
    GetMsgTimer = startTimer(100);
    QMessageBox::information(this,tr("消息"),"初始化CAN成功！");
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    QString HandleStr=ui->comboBoxDevCh->currentText();
    int start = HandleStr.indexOf("[");
    int end = HandleStr.indexOf("]");
    int CurrentHandle = HandleStr.mid(start+1,end-start-1).toUInt(NULL,16);
    int CurrentChannel = HandleStr.mid(HandleStr.indexOf("[CAN")+4,1).toUInt(NULL)-1;
    if(event->timerId()==GetMsgTimer){
        CAN_MSG msg[1000];
        int MsgNum = CAN_GetMsgWithSize(CurrentHandle,CurrentChannel,msg,1000);
        ShowMsg(msg,MsgNum);
    }
}

void MainWindow::on_pushButtonSend_clicked()
{
    QString HandleStr=ui->comboBoxDevCh->currentText();
    int start = HandleStr.indexOf("[");
    int end = HandleStr.indexOf("]");
    int CurrentHandle = HandleStr.mid(start+1,end-start-1).toUInt(NULL,16);
    int CurrentChannel = HandleStr.mid(HandleStr.indexOf("[CAN")+4,1).toUInt(NULL)-1;

    CAN_MSG msg;
    msg.ID = ui->spinBoxID->value();
    msg.ExternFlag = ui->comboBoxIDType->currentIndex();
    msg.DataLen=0;
    QStringList dataList = ui->lineEditData->text().simplified().split(" ");
    for(int i=0;i<qMin(8,dataList.length());i++){
        msg.Data[msg.DataLen++] = dataList.at(i).toUInt(NULL,16);
    }
    int ret = CAN_SendMsg(CurrentHandle,CurrentChannel,&msg,1);
    if(ret != 1){
        QMessageBox::warning(this,tr("警告"),tr("发送CAN数据失败！"));
        return;
    }
    ShowMsg(&msg,1);
}

void MainWindow::ShowMsg(CAN_MSG *pMsg,int MsgNum)
{
    for(int i=0;i<MsgNum;i++){
        QString str;
        if(pMsg[i].RemoteFlag&0x80){
            str="发送：";
        }else{
            str="接收：";
        }
        str += QString("[0x%1]").arg(pMsg[i].ID,8,16,QChar('0'));
        for(int k=0;k<pMsg[i].DataLen;k++){
            str += QString("%1 ").arg(pMsg[i].Data[k],2,16,QChar('0'));
        }
        ui->textBrowser->append(str);
    }
}
