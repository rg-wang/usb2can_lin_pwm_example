#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "usb_device.h"
#include "usb2can.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void timerEvent(QTimerEvent *event);
    void ShowMsg(CAN_MSG *pMsg,int MsgNum);
private slots:
    void on_pushButtonInit_clicked();

    void on_pushButtonSend_clicked();

private:
    Ui::MainWindow *ui;
    int GetMsgTimer;
};
#endif // MAINWINDOW_H
