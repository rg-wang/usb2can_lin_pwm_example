﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.IO;
using USB2XXX;

namespace USB2XXX_LIN_LDFParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Int32[] DevHandles = new Int32[20];
            Int32 DevHandle = 0;
            Byte LINIndex = 0;
            bool state;
            Int32 DevNum;
            string dllFilePath = "USB2XXX.dll";
            if (!File.Exists(dllFilePath))
            {
                Console.WriteLine("请先将USB2XXX.dll和libusb-1.0.dll文件复制到exe程序文件输出目录下!");
                Console.WriteLine("dll文件在‘usb2can_lin_pwm_example/sdk/libs/windows’目录下！");
                Console.WriteLine("程序是32位的就复制‘x86’目录下文件，程序是64位的就复制‘x86_64’目录下文件！");
                return;
            }
            //扫描查找设备
            DevNum = USB_DEVICE.USB_ScanDevice(DevHandles);
            if (DevNum <= 0){
                Console.WriteLine("No device connected!");
                return;
            }else{
                Console.WriteLine("Have {0} device connected!", DevNum);
            }
            DevHandle = DevHandles[0];
            //打开设备
            state = USB_DEVICE.USB_OpenDevice(DevHandle);
            if (!state){
                Console.WriteLine("Open device error!");
                return;
            }
            else{
                Console.WriteLine("Open device success!");
            }
            UInt64 LDFHandle = LDFParser.LDF_ParserFile(DevHandle,LINIndex,1, new StringBuilder("example.ldf"));
            if(LDFHandle == 0){
                Console.WriteLine("解析LDF文件失败!");
                return;
            }
            Console.WriteLine("ProtocolVersion = {0}", LDFParser.LDF_GetProtocolVersion(LDFHandle));
            Console.WriteLine("LINSpeed = {0}", LDFParser.LDF_GetLINSpeed(LDFHandle));
            StringBuilder MasterName = new StringBuilder(64);
            LDFParser.LDF_GetMasterName(LDFHandle, MasterName);
            Console.WriteLine("Master Name = {0}", MasterName);
            int FrameLen = LDFParser.LDF_GetFrameQuantity(LDFHandle);
            for (int i = 0;i < FrameLen;i++) {
                StringBuilder FrameName = new StringBuilder(64);
                if (LDFParser.LDF_PARSER_OK == LDFParser.LDF_GetFrameName(LDFHandle, i, FrameName)) {
                    StringBuilder PublisherName = new StringBuilder(64);
                    LDFParser.LDF_GetFramePublisher(LDFHandle, FrameName, PublisherName);
                    if (MasterName.Equals(PublisherName))
                    {
                        //当前帧为主机发送数据帧
                        Console.WriteLine("[MW]Frame[{0}].Name={1},Publisher={2}", i, FrameName, PublisherName);
                    }
                    else {
                        //当前帧为主机读数据帧
                        Console.WriteLine("[MR]Frame[{0}].Name={1},Publisher={2}", i, FrameName, PublisherName);
                    }
                    int SignalNum = LDFParser.LDF_GetFrameSignalQuantity(LDFHandle, FrameName);
                    for (int j = 0;j < SignalNum;j++) {
                        StringBuilder SignalName = new StringBuilder(64);
                        if (LDFParser.LDF_PARSER_OK == LDFParser.LDF_GetFrameSignalName(LDFHandle, FrameName, j, SignalName)) {
                            Console.WriteLine("\tSignal[{0}].Name={1}", j, SignalName);
                        }
                    }
                }
            }
            //主机读操作，读取从机返回的数据值
            StringBuilder ValueStr = new StringBuilder(64);
            LDFParser.LDF_ExeFrameToBus(LDFHandle, new StringBuilder("ID_DATA"),1);
            LDFParser.LDF_GetSignalValueStr(LDFHandle, new StringBuilder("ID_DATA"), new StringBuilder("Supplier_ID"), ValueStr);
            Console.WriteLine("ID_DATA.Supplier_ID={0}", ValueStr);
            LDFParser.LDF_GetSignalValueStr(LDFHandle, new StringBuilder("ID_DATA"), new StringBuilder("Machine_ID"), ValueStr);
            Console.WriteLine("ID_DATA.Machine_ID={0}", ValueStr);
            LDFParser.LDF_GetSignalValueStr(LDFHandle, new StringBuilder("ID_DATA"), new StringBuilder("Chip_ID"), ValueStr);
            Console.WriteLine("ID_DATA.Chip_ID={0}", ValueStr);
            //主机写操作，发送数据给从机
            LDFParser.LDF_SetSignalValue(LDFHandle, new StringBuilder("LIN_CONTROL"), new StringBuilder("Reg_Set_Voltage"), 13.5);
            LDFParser.LDF_SetSignalValue(LDFHandle, new StringBuilder("LIN_CONTROL"), new StringBuilder("Ramp_Time"), 3);
            LDFParser.LDF_SetSignalValue(LDFHandle, new StringBuilder("LIN_CONTROL"), new StringBuilder("Cut_Off_Speed"), 4);
            LDFParser.LDF_SetSignalValue(LDFHandle, new StringBuilder("LIN_CONTROL"), new StringBuilder("Exc_Limitation"), 15.6);
            LDFParser.LDF_SetSignalValue(LDFHandle, new StringBuilder("LIN_CONTROL"), new StringBuilder("Derat_Shift"), 2);
            LDFParser.LDF_SetSignalValue(LDFHandle, new StringBuilder("LIN_CONTROL"), new StringBuilder("MM_Request"), 2);
            LDFParser.LDF_SetSignalValue(LDFHandle, new StringBuilder("LIN_CONTROL"), new StringBuilder("Reg_Blind"), 1);
            //LDF_ExeFrameToBus(LDFHandle, "LIN_CONTROL");
            //执行调度表
            LDFParser.LDF_ExeSchToBus(LDFHandle, new StringBuilder("Nissan"),1);
            LDFParser.LDF_GetSignalValueStr(LDFHandle, new StringBuilder("LIN_STATE"), new StringBuilder("MM_State"), ValueStr);
            Console.WriteLine("LIN_STATE.MM_State={0}", ValueStr);
            LDFParser.LDF_GetSignalValueStr(LDFHandle, new StringBuilder("LIN_STATE"), new StringBuilder("Exc_Duty_Cycle"), ValueStr);
            Console.WriteLine("LIN_STATE.Exc_Duty_Cycle={0}", ValueStr);
            LDFParser.LDF_GetSignalValueStr(LDFHandle, new StringBuilder("LIN_STATE"), new StringBuilder("Exc_Current"), ValueStr);
            Console.WriteLine("LIN_STATE.Exc_Current={0}", ValueStr);
            LDFParser.LDF_GetSignalValueStr(LDFHandle, new StringBuilder("LIN_STATE"), new StringBuilder("iStARS_Voltage"), ValueStr);
            Console.WriteLine("LIN_STATE.iStARS_Voltage={0}", ValueStr);
        }
    }
}
