# USB2CAN范例工程说明

对每个工程做简要功能说明，可以根据自己的实际需求选择参考对应工程。范例程序中使用到的二次开发接口函数说明可以参考：[接口函数说明文档](http://www.toomoss.cn/api/index.html)

## USB2XXX_CANTest
实现最基本的CAN数据收发，将CAN1和CAN2对接后，运行程序可以看到两个通道收发的数据。

## USB2XXX_CAN_SchTest
通过调度表模式发送数据，该方式可以比较精确的控制CAN消息发送间隔时间。

## USB2XXX_CAN_UDSTest
基于CAN UDS实现的数据读写测试程序，需要使用CAN UDS通信可以参考该程序。

## USB2XXX_CAN_DBCParserTest
实现解析DBC文件，并将CAN原始数据通过DBC文件解析为物理数据，或者将物理数据转换为原始数据。

## USB2XXX_CAN_MultiThreadTest
CAN多线程模式收发数据，需要使用多线程收发数据可以参考该代码。

## USB2XXX_CANBootloader
根据自定义CAN通信协议实现的CAN Bootloader界面程序。