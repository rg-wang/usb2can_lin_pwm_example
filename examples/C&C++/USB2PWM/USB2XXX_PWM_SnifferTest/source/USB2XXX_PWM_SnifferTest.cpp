/**
 * @file   USB2XXX_PWM_SnifferTest.cpp
 * @brief  PWM监控测试程序
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-12-06
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "usb_device.h"
#include "usb2pwm.h"
#if _WIN32
#include <windows.h>
#pragma comment(lib,"USB2XXX.lib")
#endif
int main(int argc, const char* argv[])
{
    DEVICE_INFO DevInfo;
    int DevHandle[10];
    bool state;
    int ret;
    //扫描设备并获取设备号
    ret = USB_ScanDevice(DevHandle);
    if(ret <= 0){
        printf("No device connected!\n");
        return 0;
    }
    //打开设备
    state = USB_OpenDevice(DevHandle[0]);
    if(!state){
        printf("Open device error!\n");
        return 0;
    }
    //获取设备信息
    char FuncStr[256]={0};
    state = DEV_GetDeviceInfo(DevHandle[0],&DevInfo,FuncStr);
    if(!state){
        printf("Get device infomation error!\n");
        return 0;
    }else{
        printf("Firmware Info:\n");
        printf("    Name:%s\n",DevInfo.FirmwareName);
        printf("    Build Date:%s\n",DevInfo.BuildDate);
        printf("    Version:v%d.%d.%d\n",(DevInfo.FirmwareVersion>>24)&0xFF,(DevInfo.FirmwareVersion>>16)&0xFF,DevInfo.FirmwareVersion&0xFFFF);
        printf("    Version:v%d.%d.%d\n",(DevInfo.HardwareVersion>>24)&0xFF,(DevInfo.HardwareVersion>>16)&0xFF,DevInfo.HardwareVersion&0xFFFF);
        printf("    Functions:%08X\n",DevInfo.Functions);
        printf("    Functions:%s\n",FuncStr);
    }
    int Channel = 0;
    int TimePrecUs = 10;//探测PWM时间精度
    ret = PWM_CAP_Init(DevHandle[0], Channel,TimePrecUs);
    if(ret != PWM_SUCCESS){
        printf("Initialize pwm sniffer faild!\n");
        return ret;
    }else{
        printf("Initialize pwm sniffer sunccess!\n");
    }
    //循环读取监控到的数据
    for(int t=0;t<10;t++){
        PWM_CAP_DATA PWMCapData;
        ret = PWM_CAP_GetData(DevHandle[0], Channel,&PWMCapData);
        if(ret == PWM_SUCCESS){
            if (((PWMCapData.HighValue + PWMCapData.LowValue) > 0)&&(PWMCapData.HighValue<0xFFFF)&&(PWMCapData.LowValue<0xFFFF))
            {
                printf("频率 = %d Hz\n", 1000000 / ((PWMCapData.HighValue + PWMCapData.LowValue)*TimePrecUs));
                printf("占空比 = %d %%\n", (100 * PWMCapData.HighValue) / (PWMCapData.HighValue + PWMCapData.LowValue));
            }
            else
            {
                printf("未检测到PWM信号\n");
            }
        }else{
            printf("get pwm data faild!");
            break;
        }
#if _WIN32
        Sleep(1000);
#else
        usleep(1000*1000);
#endif
    }
    //停止探测
    ret = PWM_CAP_Stop(DevHandle[0], Channel);
    if(ret != PWM_SUCCESS){
        printf("Stop pwm sniffer faild!\n");
        return ret;
    }else{
        printf("Stop pwm sniffer sunccess!\n");
    }
    //关闭设备
    USB_CloseDevice(DevHandle[0]);
	return 0;
}

