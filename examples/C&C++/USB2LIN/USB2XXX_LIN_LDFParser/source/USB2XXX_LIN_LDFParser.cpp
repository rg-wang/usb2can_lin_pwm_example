/**
 * @file   USB2XXX_LIN_LDFParser.cpp
 * @brief  LDF文件解析范例程序
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-12-03
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ldf_parser.h"
#include "usb_device.h"
#include "usb2lin_ex.h"
#if _WIN32
#include <windows.h>
#pragma comment(lib,"USB2XXX.lib")
#endif

int main(int argc, const char* argv[])
{
    int DevHandle[20];
    int LINIndex = 0;//LIN1
    //扫描设备并获取设备号
    int ret = USB_ScanDevice(DevHandle);
    if(ret <= 0){
        printf("No device connected!\n");
        return 0;
    }
    if (!USB_OpenDevice(DevHandle[0])) {
        printf("Open device error!\n");
        return 0;
    }
    printf("DevHandle = %08X\n", DevHandle[0]);
    long long LDFHandle = LDF_ParserFile(DevHandle[0],LINIndex,1, "example.ldf");
    if(LDFHandle == NULL)
    {
        printf("Parser LDF File Error\n");
        return 0;
    }
    printf("ProtocolVersion = %d\n", LDF_GetProtocolVersion(LDFHandle));
    printf("LINSpeed = %d\n", LDF_GetLINSpeed(LDFHandle));
    char MasterName[128];
    LDF_GetMasterName(LDFHandle, MasterName);
    printf("Master Name = %s\n", MasterName);
    int FrameLen = LDF_GetFrameQuantity(LDFHandle);
    for (int i = 0;i < FrameLen;i++) {
        char FrameName[128];
        if (LDF_PARSER_OK == LDF_GetFrameName(LDFHandle, i, FrameName)) {
            char PublisherName[64];
            LDF_GetFramePublisher(LDFHandle, FrameName, PublisherName);
            if (strcmp(MasterName, PublisherName) == 0) {
                //打印主机写帧名称
                printf("[MW]Frame[%d].Name=%s  Publisher=%s\n", i, FrameName, PublisherName);
            }
            else {
                //打印主机读帧名称
                printf("[MR]Frame[%d].Name=%s  Publisher=%s\n", i, FrameName, PublisherName);
            }
            int SignalNum = LDF_GetFrameSignalQuantity(LDFHandle, FrameName);
            for (int j = 0;j < SignalNum;j++) {
                char SignalName[128];
                if (LDF_PARSER_OK == LDF_GetFrameSignalName(LDFHandle, FrameName, j, SignalName)) {
                    printf("\tSignal[%d].Name=%s\n", j, SignalName);
                }
            }
        }
    }
    //执行帧之后获取读到的信号值
    char ValueStr[128] = {0};
    ret = LDF_ExeFrameToBus(LDFHandle, "ID_DATA",1);//从总线读取数据，该函数是读数据还是写数据，取决于帧的发布者是不是主机
    if(ret == LDF_PARSER_OK){
        LDF_GetSignalValueStr(LDFHandle, "ID_DATA", "Machine_ID", ValueStr);
        printf("ID_DATA.Machine_ID=%s\n", ValueStr);
        LDF_GetSignalValueStr(LDFHandle, "ID_DATA", "Chip_ID", ValueStr);
        printf("ID_DATA.Chip_ID=%s\n", ValueStr);
    }else{
        printf("Read Data From Bus Error! ret = %d\n",ret);
    }

    //设置信号值
    LDF_SetSignalValue(LDFHandle, "LIN_CONTROL", "Reg_Set_Voltage", 13.5);
    LDF_SetSignalValue(LDFHandle, "LIN_CONTROL", "Ramp_Time", 3);
    LDF_SetSignalValue(LDFHandle, "LIN_CONTROL", "Cut_Off_Speed", 4);
    LDF_SetSignalValue(LDFHandle, "LIN_CONTROL", "Exc_Limitation", 15.6);
    LDF_SetSignalValue(LDFHandle, "LIN_CONTROL", "Derat_Shift", 2);
    LDF_SetSignalValue(LDFHandle, "LIN_CONTROL", "MM_Request", 2);
    LDF_SetSignalValue(LDFHandle, "LIN_CONTROL", "Reg_Blind", 1);
    LDF_ExeFrameToBus(LDFHandle, "LIN_CONTROL",1);//主机写数据到总线，该函数是读数据还是写数据，取决于帧的发布者是不是主机
    //ִ执行调度表
    LDF_ExeSchToBus(LDFHandle, "Nissan",1);
    ret = LDF_GetSignalValueStr(LDFHandle, "LIN_STATE", "MM_State", ValueStr);
    if(ret == LDF_PARSER_OK){
        printf("LIN_STATE.MM_State=%s\n", ValueStr);
    }
    ret = LDF_GetSignalValueStr(LDFHandle, "LIN_STATE", "Exc_Duty_Cycle", ValueStr);
    if(ret == LDF_PARSER_OK){
        printf("LIN_STATE.Exc_Duty_Cycle=%s\n", ValueStr);
    }
    ret = LDF_GetSignalValueStr(LDFHandle, "LIN_STATE", "Exc_Current", ValueStr);
    if(ret == LDF_PARSER_OK){
        printf("LIN_STATE.Exc_Current=%s\n", ValueStr);
    }
    ret = LDF_GetSignalValueStr(LDFHandle, "LIN_STATE", "iStARS_Voltage", ValueStr);
    if(ret == LDF_PARSER_OK){
        printf("LIN_STATE.iStARS_Voltage=%s\n", ValueStr);
    }
    LIN_EX_MSG msg[100];
    ret = LDF_GetRawMsg(LDFHandle,msg,100);
    printf("ret = %d\n",ret);
    //关闭设备
    USB_CloseDevice(DevHandle[0]);
	return 0;
}

