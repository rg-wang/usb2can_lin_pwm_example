"""
文件说明：USB2XXX LIN相关函数测试程序
更多帮助：www.toomoss.com
"""
from ctypes import *
import platform
from time import sleep
from usb_device import *
from usb2lin_ex import *


if __name__ == '__main__': 
    LINMasterIndex = 1
    DevHandles = (c_uint * 20)()
    # Scan device
    ret = USB_ScanDevice(byref(DevHandles))
    if(ret == 0):
        print("No device connected!")
        exit()
    else:
        print("Have %d device connected!"%ret)
    # Open device
    ret = USB_OpenDevice(DevHandles[0])
    if(bool(ret)):
        print("Open device success!")
    else:
        print("Open device faild!")
        exit()
    # Get device infomation
    USB2XXXInfo = DEVICE_INFO()
    USB2XXXFunctionString = (c_char * 256)()
    ret = DEV_GetDeviceInfo(DevHandles[0],byref(USB2XXXInfo),byref(USB2XXXFunctionString))
    if(bool(ret)):
        print("USB2XXX device infomation:")
        print("--Firmware Name: %s"%bytes(USB2XXXInfo.FirmwareName).decode('ascii'))
        print("--Firmware Version: v%d.%d.%d"%((USB2XXXInfo.FirmwareVersion>>24)&0xFF,(USB2XXXInfo.FirmwareVersion>>16)&0xFF,USB2XXXInfo.FirmwareVersion&0xFFFF))
        print("--Hardware Version: v%d.%d.%d"%((USB2XXXInfo.HardwareVersion>>24)&0xFF,(USB2XXXInfo.HardwareVersion>>16)&0xFF,USB2XXXInfo.HardwareVersion&0xFFFF))
        print("--Build Date: %s"%bytes(USB2XXXInfo.BuildDate).decode('ascii'))
        print("--Serial Number: ",end='')
        for i in range(0, len(USB2XXXInfo.SerialNumber)):
            print("%08X"%USB2XXXInfo.SerialNumber[i],end='')
        print("")
        print("--Function String: %s"%bytes(USB2XXXFunctionString.value).decode('ascii'))
    else:
        print("Get device infomation faild!")
        exit()

    # 初始化配置主LIN
    ret = LIN_EX_Init(DevHandles[0],LINMasterIndex,19200,LIN_EX_MASTER)
    if ret != LIN_EX_SUCCESS:
        print("Config Master LIN failed!")
        exit()
    else:
        print("Config Master LIN Success!")
    #发送BREAK信号，一般用于唤醒设备
    ret = LIN_EX_MasterBreak(DevHandles[0],LINMasterIndex);
    if ret != LIN_EX_SUCCESS:
        print("Send LIN break failed!")
        exit()
    else:
        print("Send LIN break success!")
    sleep(0.01)

    # 主机写数据
    data_buffer = (c_byte*8)(0x7F,0x06,0xB4,0x11,0x5A,0x5A,0xFF,0x03)
    ret = LIN_EX_MasterWrite(DevHandles[0],LINMasterIndex,0x3C,data_buffer,8,0)
    if ret != LIN_EX_SUCCESS:
        print("LIN ID[0x%02X] write data failed!"%0x3C)
        exit()
    else:
        print("LIN ID[0x%02X] write data success"%0x3C)
    sleep(0.01)
    

    # 主机读数据
    ret = LIN_EX_MasterRead(DevHandles[0],LINMasterIndex,0x3D,data_buffer)
    if ret <= 0:
        print("LIN read data failed! ret = %d"%ret)
        exit()
    else:
        print("LIN ID[0x%02X] read data:"%0x3D,end='')
        for i in range(ret):
            print("0x%02X "%(data_buffer[i]&0xFF),end='')
        print("")

    # Close device
    ret = USB_CloseDevice(DevHandles[0])
    if(bool(ret)):
        print("Close device success!")
    else:
        print("Close device faild!")
        exit()
