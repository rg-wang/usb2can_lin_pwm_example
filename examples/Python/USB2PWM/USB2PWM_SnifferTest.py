"""
文件说明：USB2XXX PWM相关函数测试程序
更多帮助：www.toomoss.com
"""
from ctypes import *
import platform
from time import sleep
from usb_device import *
from usb2pwm import *

if __name__ == '__main__': 
    DevIndex = 0
    LIN1=0
    LIN2=1
    DevHandles = (c_int * 20)()
    # Scan device
    ret = USB_ScanDevice(byref(DevHandles))
    if(ret == 0):
        print("No device connected!")
        exit()
    else:
        print("Have %d device connected!"%ret)
    # Open device
    ret = USB_OpenDevice(DevHandles[DevIndex])
    if(bool(ret)):
        print("Open device success!")
    else:
        print("Open device faild!")
        exit()
    # Get device infomation
    USB2XXXInfo = DEVICE_INFO()
    USB2XXXFunctionString = (c_char * 256)()
    ret = DEV_GetDeviceInfo(DevHandles[DevIndex],byref(USB2XXXInfo),byref(USB2XXXFunctionString))
    if(bool(ret)):
        print("USB2XXX device infomation:")
        print("--Firmware Name: %s"%bytes(USB2XXXInfo.FirmwareName).decode('ascii'))
        print("--Firmware Version: v%d.%d.%d"%((USB2XXXInfo.FirmwareVersion>>24)&0xFF,(USB2XXXInfo.FirmwareVersion>>16)&0xFF,USB2XXXInfo.FirmwareVersion&0xFFFF))
        print("--Hardware Version: v%d.%d.%d"%((USB2XXXInfo.HardwareVersion>>24)&0xFF,(USB2XXXInfo.HardwareVersion>>16)&0xFF,USB2XXXInfo.HardwareVersion&0xFFFF))
        print("--Build Date: %s"%bytes(USB2XXXInfo.BuildDate).decode('ascii'))
        print("--Serial Number: ",end='')
        for i in range(0, len(USB2XXXInfo.SerialNumber)):
            print("%08X"%USB2XXXInfo.SerialNumber[i],end='')
        print("")
        print("--Function String: %s"%bytes(USB2XXXFunctionString.value).decode('ascii'))
    else:
        print("Get device infomation faild!")
        exit()
    # 初始化PWM
    ret = PWM2_Init(DevHandles[DevIndex],LIN1,1000,0,1000,250)
    if ret != PWM_SUCCESS:
        print("Initialize pwm faild!")
        exit()
    else:
        print("Initialize pwm sunccess!")
    # 启动PWM,并一直输出PWM
    RunTimeOfUs = 0 #一直输出
    ret = PWM2_Start(DevHandles[DevIndex],LIN1,RunTimeOfUs)
    if(ret != PWM_SUCCESS):
        print("Start pwm faild!")
        exit()
    else:
        print("Start pwm sunccess!")
    # 初始化PWM监控
    TimePrecUs = 10 #PWM监控时间精度，单位为微秒
    ret = PWM_CAP_Init(DevHandles[DevIndex],LIN2,TimePrecUs)
    if(ret != PWM_SUCCESS):
        print("Start pwm sniffer faild!")
        exit()
    else:
        print("Start pwm sniffer sunccess!")
    # 循环获取数据
    for t in range(0,10):
        PWMData = PWM_CAP_DATA()
        ret = PWM_CAP_GetData(DevHandles[DevIndex],LIN2,byref(PWMData))
        if (ret != PWM_SUCCESS):
            print("pwm cap data faild!")
        else:
            print(f"HighValue={PWMData.HighValue*TimePrecUs} us")
            print(f"LowValue={PWMData.LowValue*TimePrecUs} us")
            if (((PWMData.HighValue + PWMData.LowValue) > 0)and (PWMData.HighValue < 0xFFFF)and (PWMData.LowValue < 0xFFFF)):
                pwm_cap_freq = 1000000 / ((PWMData.HighValue + PWMData.LowValue)*TimePrecUs)
                pwm_cap_duty = (100 * PWMData.HighValue) / (PWMData.HighValue + PWMData.LowValue)
                print(f"cap freq:{pwm_cap_freq}Hz cap duty:{pwm_cap_duty}%")
            else:
                print("未检测到PWM信号")
        sleep(0.01)
    #停止监控
    PWM_CAP_Stop(DevHandles[DevIndex],1)
    exit()

