QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32{
    message($$QT_ARCH)
    contains(QT_ARCH, i386) {
        LIBS += -L$$PWD/libs/windows/x86 -lUSB2XXX
    } else {
        LIBS += -L$$PWD/libs/windows/x86_64 -lUSB2XXX
    }
}

unix:!macx{
    message($$QMAKE_HOST.arch)
    unix:contains(QMAKE_HOST.arch, x86_64){
        LIBS += -L$$PWD/libs/linux/x86_64 -lUSB2XXX
    }
    unix:contains(QMAKE_HOST.arch, x86){
        LIBS += -L$$PWD/libs/linux/x86 -lUSB2XXX
    }
    unix:contains(QMAKE_HOST.arch, aarch64){
        LIBS += -L$$PWD/libs/linux/aarch64 -lUSB2XXX
    }
    unix:contains(QMAKE_HOST.arch, armv7){
        LIBS += -L$$PWD/libs/linux/armv7 -lUSB2XXX
    }
    unix:contains(QMAKE_HOST.arch, mips64){
        LIBS += -L$$PWD/libs/linux/mips64 -lUSB2XXX
    }
}
macx{
    message($$QT_ARCH)
    unix:contains(QT_ARCH, x86_64){
        LIBS += -L$$PWD/libs/mac_os/x86_64 -lUSB2XXX
    }
    unix:contains(QT_ARCH, arm64){
        LIBS += -L$$PWD/libs/mac_os/arm64 -lUSB2XXX
    }
}


SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h \
    usb2can.h \
    usb2canfd.h \
    usb_device.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
