  /**
 * @file   USB2XXX_CAN_UDSTest.cpp
 * @brief  CAN UDS测试程序，该程序可以适用于普通CAN或者CANFD
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-11-18
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "usb_device.h"
#include "usb2can.h"
#include "can_uds.h"
#include "usb2canfd.h"

#if _WIN32
#include <windows.h>
#pragma comment(lib,"USB2XXX.lib")
#endif

#define GET_FIRMWARE_INFO   1

int main(int argc, const char* argv[])
{
#if GET_FIRMWARE_INFO
    DEVICE_INFO DevInfo;
#endif
    int DevHandle[10];
    int CANIndexMaster = 0;
    int CANIndexSlave = 1;
    int DEVIndex = 0;
    bool state;
    int ret;
    //扫描查找设备
    ret = USB_ScanDevice(DevHandle);
    if(ret <= 0){
        printf("No device connected!\r\n");
        return 0;
    }
    //打开设备
    state = USB_OpenDevice(DevHandle[DEVIndex]);
    if(!state){
        printf("Open device error!\r\n");
        return 0;
    }
#if GET_FIRMWARE_INFO
    char FunctionStr[256]={0};
    //获取固件信息
    state = DEV_GetDeviceInfo(DevHandle[DEVIndex],&DevInfo,FunctionStr);
    if(!state){
        printf("Get device infomation error!\r\n");
        return 0;
    }else{
        printf("Firmware Info:\r\n");
	    printf("Firmware Name:%s\r\n",DevInfo.FirmwareName);
        printf("Firmware Build Date:%s\r\n",DevInfo.BuildDate);
        printf("Firmware Version:v%d.%d.%d\r\n",(DevInfo.FirmwareVersion>>24)&0xFF,(DevInfo.FirmwareVersion>>16)&0xFF,DevInfo.FirmwareVersion&0xFFFF);
        printf("Hardware Version:v%d.%d.%d\r\n",(DevInfo.HardwareVersion>>24)&0xFF,(DevInfo.HardwareVersion>>16)&0xFF,DevInfo.HardwareVersion&0xFFFF);
	    printf("Firmware Functions:%s\r\n",FunctionStr);
    }
#endif
    if((DevHandle[DEVIndex]&0xFF000000)==0x53000000){
        CANFD_INIT_CONFIG CANFDConfig;
        ret = CANFD_GetCANSpeedArg(DevHandle[DEVIndex],&CANFDConfig,125000,125000); //获取CANFD波特率参数  
        if(ret == CANFD_SUCCESS){
            CANFDConfig.ResEnable = 1;//接入终端电阻
            ret = CANFD_Init(DevHandle[DEVIndex],CANIndexMaster,&CANFDConfig);
            if(ret != CANFD_SUCCESS){
                printf("CANFD Init Error!\n");
                return 0;
            }else{
                printf("CANFD Init Success!\n");
            }
        }else{
            printf("CANFD Get speed  arguments failed!\n");
            return 0;
        }
    }else{
        //初始化配置CAN
        CAN_INIT_CONFIG CANConfig;
        ret = CAN_GetCANSpeedArg(DevHandle[DEVIndex],&CANConfig,125000);  //获取CAN波特率参数  
        if(ret == CAN_SUCCESS){
            CANConfig.CAN_Mode = 0x80;//正常模式，接入终端电阻
            ret = CAN_Init(DevHandle[DEVIndex],CANIndexMaster,&CANConfig);
            if(ret != CAN_SUCCESS){
                printf("Config CAN failed!\r\n");
                return 0;
            }else{
                printf("Config CAN Success!\r\n");
            }
        }else{
            printf("CAN Get speed  arguments failed!\n");
            return 0;
        }
    }
    for (int t = 0; t < 2; t++){
        //设置请求数据
        CAN_UDS_ADDR UDSAddr;
        UDSAddr.Flag = 0;//使用标准帧
        UDSAddr.AddrFormats = 0;
        UDSAddr.ReqID = 0x733;
        UDSAddr.ResID = 0x73B;
        UDSAddr.MaxDLC = 8;
        uint8_t req_data[]={0x22,0xF1,0x13};//第一字节为服务ID(SID)，后面的数据为对应的参数
        uint8_t res_data[4096]={0};
        ret = CAN_UDS_Request(DevHandle[DEVIndex],CANIndexMaster,&UDSAddr,req_data,sizeof(req_data));
        if(ret != CAN_UDS_OK){
            printf("CAN UDS request failed! %d\r\n",ret);
        }else{
            printf("Request:");
            for(int i=0;i<sizeof(req_data);i++){
                printf(" %02X",req_data[i]);
            }
            printf("\r\n");
        }
        //获取响应数据，有可能因为设备会进入睡眠模式，若第一次无法正常获取响应，可以延时100ms之后再次请求获取响应
        ret = CAN_UDS_Response(DevHandle[DEVIndex],CANIndexMaster,&UDSAddr,res_data,200);
        if(ret <= CAN_UDS_OK){
            printf("CAN UDS response failed! %d\r\n",ret);
        }else{
            printf("Response:");
            for(int i=0;i<ret;i++){
                printf(" %02X",res_data[i]);
            }
            printf("\r\n");
        }
        //延时
#if _WIN32
        Sleep(200);
#else
        usleep(200*1000);
#endif
    }
	return 0;
}

