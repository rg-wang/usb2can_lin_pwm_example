#ifndef __DLL_DEBUG_H_
#define __DLL_DEBUG_H_

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#define WINAPI
#endif

typedef  int (WINAPI* DEBUG_HANDLE)(char* pData);//接收数据回掉函数

#ifdef __cplusplus
extern "C"
{
#endif

void WINAPI DEBUG_SetHandle(DEBUG_HANDLE pHandle);

#ifdef __cplusplus
}
#endif
#endif

